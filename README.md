# How to get from Pskov to Tartu and back by public transportation

Collection of info about trains and buses from Pskov (Koidula) to Tartu.

## Train

http://3ty.ru/rasp/0004196.html

| Поезд  |	Дни  | Из      | В       | Отправл. | Прибытие | 
| ------ | ----- | ------- | ------- | -------- | -------- |
| ПВ0381 | КПнСВ | Койдула | Тарту	 | 06:02 (07:02) | 7:19 | 
| ПВ0383 | СВ    | Койдула | Тарту	 | 08:11 (09:11) | 9:28 | 
| ПВ0382 | КПн   | Тарту   | Койдула | 10:26 | 11:43 (12:43) |
| ПВ0387 |       | Койдула | Тарту	 | 15:51 (16:51) | 17:08 |
| ПВ0386 |       | Тарту   | Койдула | 17:19 | 18:36 (19:36) |

## Bus

### Ecolines: среда, субб

http://ecolines.ru/ru/predlozhenija/456-pskov-tartu

* Pskov -> Tartu: 6:55 -> 10:55
* Tartu -> Pskov: 17:05 -> 21:05

1430 rub

### SAIMAAN LIIKENNE

600 rub

Pskov -> Tartu 8:20 -> 11:35

Tartu -> Pskov 10:35 -> 14:05

http://www.sltravelspb.ru/e-stoniya/rejsovy-e-avtobusy-v-tallin/

### Simple Express

http://pilet.simpleexpress.eu/ru/poezdku-raspisanija/tartu/koidula?Date=01-14-2016

* Koidula -> Tartu: 10:30 -> 12:45
* Tartu -> Koidula: 16:15 -> 18:25

1 euro

### tPilet

Many bus options, also with connections

http://www.tpilet.ee/ru/raspisanija/tartu/koidula?Scope=All&Date=01-12-2016&Transfer=LessThan90